
# Case Study - Table Filter

Monorepo case study project in Angular and Symfony PHP with login and data view. Running on docker with docker-compose. 

## Prerequisites

- Docker
- Docker-Compose

**Only Windows:** please set ```git config --global core.autocrlf input``` before checkout to avoid problems with line endings


## Install

The project contains a docker-compose file, which sets up the containers for Symfony, Angular and MySQL.
The products are loaded automatically into the db via fixtures on startup.

To start the app run:
```sh
docker-compose up -d
```

The angular app: http://localhost:4200/

The symfony api: http://localhost:8000/

## Login

Username: demo

Password: 123

## Dependencies

Backend:

Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle (JWT-Token-Handling)

Nelmio\CorsBundle\NelmioCorsBundle (CORS)

Frontend:

@angular/material (UI-Kit)
