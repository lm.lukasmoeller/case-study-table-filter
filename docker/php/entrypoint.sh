#!/bin/sh

#wait for db to be ready
sleep 20

composer install
#create case_study db
php bin/console doctrine:database:create
#migrate products table
php bin/console doctrine:migrations:migrate
#load products
php bin/console doctrine:fixtures:load --no-interaction

#start server
exec "$@"