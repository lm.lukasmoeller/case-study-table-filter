import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
	GetProductsApiOptions,
	GetProductsResponse,
} from 'src/app/models/product';
import { environment } from '../../../../environments/environment';
import { AuthService } from '../../auth/auth.service';

@Injectable({
	providedIn: 'root',
})
export class ProductsService {
	constructor(private http: HttpClient, private _authService: AuthService) {}

	public getProducts$(
		options: GetProductsApiOptions
	): Observable<GetProductsResponse> {
		return this.http.get<GetProductsResponse>(
			environment.api_url + '/products',
			{
				params: new HttpParams()
					.set('filter', options.filter ?? '')
					.set('pageNumber', options.pageNumber.toString() ?? '')
					.set('pageSize', options.pageSize.toString() ?? ''),
				headers: {
					Authorization: 'Bearer ' + this._authService.token,
				},
			}
		);
	}
}
