import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PostLoginResponse } from 'src/app/models/auth';

@Injectable({
	providedIn: 'root',
})
export class AuthService implements OnDestroy {
	private _tokenSub = new BehaviorSubject<string>('');
	private _authSub = new BehaviorSubject<boolean>(false);

	public token$ = this._tokenSub.asObservable();

	public get token() {
		return this._tokenSub.value;
	}

	public get isAuthenticated$(): Observable<boolean> {
		return this._authSub.asObservable();
	}

	constructor(private http: HttpClient) {
		const storedToken = localStorage.getItem('token');
		if (storedToken != null) {
			this._tokenSub.next(storedToken);
		}

		this._authSub.next(storedToken != null);
	}

	ngOnDestroy(): void {
		this._authSub.complete();
		this._tokenSub.complete();
	}

	public login$(
		username: string,
		password: string
	): Observable<PostLoginResponse> {
		return this.http
			.post<PostLoginResponse>(
				environment.api_url + '/login',
				{
					username: username,
					password: password,
				},
				{
					headers: {
						'Content-Type': 'application/json',
					},
				}
			)
			.pipe(
				tap((res) => {
					localStorage.setItem('token', res.token);
					this._tokenSub.next(res.token);
					this._authSub.next(true);
				})
			);
	}

	public logout(): void {
		localStorage.removeItem('token');
		this._tokenSub.next('');
		this._authSub.next(false);
	}
}
