import {
	Component,
	ElementRef,
	OnDestroy,
	OnInit,
	ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, take, takeUntil, tap } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
	public loginValid = true;

	private _destroySub$ = new Subject<void>();
	private readonly returnUrl: string;

	@ViewChild('usernameInp') username!: ElementRef;
	@ViewChild('passwordInp') password!: ElementRef;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _authService: AuthService
	) {
		this.returnUrl =
			this._route.snapshot.queryParams['returnUrl'] || '/products';
	}

	ngOnInit(): void {
		this._authService.isAuthenticated$
			.pipe(
				tap((isAuthenticated: boolean) => isAuthenticated),
				takeUntil(this._destroySub$)
			)
			.subscribe((_) => this._router.navigateByUrl(this.returnUrl));
	}

	ngOnDestroy(): void {
		this._destroySub$.next();
	}

	public onSubmit(): void {
		this.loginValid = true;

		this._authService
			.login$(
				this.username.nativeElement.value,
				this.password.nativeElement.value
			)
			.pipe(take(1))
			.subscribe({
				next: (response) => {
					if (response.token) {
						localStorage.setItem('token', response.token);
						this.loginValid = true;
						this._router.navigateByUrl('');
					} else {
						this.loginValid = false;
					}
				},
				error: (_) => (this.loginValid = false),
			});
	}
}
