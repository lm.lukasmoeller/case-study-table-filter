import {
	AfterViewInit,
	Component,
	ElementRef,
	OnInit,
	ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged, fromEvent, tap } from 'rxjs';
import { ProductsService } from 'src/app/services/api/products/products.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProductsDataSource } from './products.data-source';

@Component({
	selector: 'app-products',
	templateUrl: './products.component.html',
	styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit, AfterViewInit {
	public displayedColumns = [
		'id',
		'gtin',
		'product_name',
		'product_desc',
		'price',
		'currency',
		'category',
		'gender',
		'quantity',
		'size',
		'style',
		'color',
		'url',
		'image',
		'image_additional',
		'additional',
		'source_video',
	];
	public defaultSize = 10;
	public dataSource: ProductsDataSource;

	@ViewChild(MatPaginator) paginator!: MatPaginator;
	@ViewChild('filter') filterInput!: ElementRef;

	constructor(
		private _authService: AuthService,
		private _router: Router,
		_productsService: ProductsService
	) {
		this.dataSource = new ProductsDataSource(_productsService);
	}

	ngOnInit(): void {
		this.dataSource.loadProducts({
			filter: '',
			pageNumber: 0,
			pageSize: this.defaultSize,
		});
	}

	ngAfterViewInit() {
		this.dataSource.totalItems$.subscribe((count) => {
			this.paginator.length = count;
		});

		this.paginator.page.pipe(tap(() => this._loadProductsPage())).subscribe();

		fromEvent(this.filterInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(500),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this._loadProductsPage();
				})
			)
			.subscribe();
	}

	public getValue(event: Event): string {
		return (event.target as HTMLInputElement).value;
	}

	public logout(): void {
		this._authService.logout();
		this._router.navigateByUrl('/login');
	}

	private _loadProductsPage() {
		this.dataSource.loadProducts({
			filter: this.filterInput.nativeElement.value,
			pageNumber: this.paginator.pageIndex,
			pageSize: this.paginator.pageSize,
		});
	}
}
