import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, finalize, Observable } from 'rxjs';
import { GetProductsApiOptions, Product } from 'src/app/models/product';
import { ProductsService } from 'src/app/services/api/products/products.service';

export class ProductsDataSource implements DataSource<Product> {
	private _productsSubject = new BehaviorSubject<Array<Product>>([]);
	private _loadingSubject = new BehaviorSubject<boolean>(false);
	private _totalItemsSubject = new BehaviorSubject<number>(0);

	public loading$ = this._loadingSubject.asObservable();
	public totalItems$ = this._totalItemsSubject.asObservable();

	constructor(private _productsService: ProductsService) {}

	connect(collectionViewer: CollectionViewer): Observable<readonly Product[]> {
		return this._productsSubject.asObservable();
	}
	disconnect(collectionViewer: CollectionViewer): void {
		this._productsSubject.complete();
		this._loadingSubject.complete();
		this._totalItemsSubject.complete();
	}

	public loadProducts(options: GetProductsApiOptions) {
		this._loadingSubject.next(true);

		this._productsService
			.getProducts$(options)
			.pipe(finalize(() => this._loadingSubject.next(false)))
			.subscribe((res) => {
				this._totalItemsSubject.next(res.totalItems);
				this._productsSubject.next(res.products);
			});
	}
}
