<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220906201618 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, gtin INT NOT NULL, product_name VARCHAR(255) NOT NULL, product_desc VARCHAR(255) DEFAULT NULL, price DOUBLE PRECISION NOT NULL, currency VARCHAR(255) NOT NULL, category VARCHAR(255) DEFAULT NULL, gender VARCHAR(255) DEFAULT NULL, quantity INT DEFAULT NULL, size VARCHAR(255) DEFAULT NULL, style VARCHAR(255) DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, image_additional VARCHAR(255) DEFAULT NULL, additional VARCHAR(255) DEFAULT NULL, source_video VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE product');
    }
}
