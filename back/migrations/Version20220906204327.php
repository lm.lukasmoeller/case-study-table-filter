<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220906204327 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product CHANGE product_name product_name LONGTEXT NOT NULL, CHANGE product_desc product_desc LONGTEXT DEFAULT NULL, CHANGE image image LONGTEXT DEFAULT NULL, CHANGE image_additional image_additional LONGTEXT DEFAULT NULL, CHANGE additional additional LONGTEXT DEFAULT NULL, CHANGE source_video source_video LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product CHANGE product_name product_name VARCHAR(255) NOT NULL, CHANGE product_desc product_desc VARCHAR(255) DEFAULT NULL, CHANGE image image VARCHAR(255) DEFAULT NULL, CHANGE image_additional image_additional VARCHAR(255) DEFAULT NULL, CHANGE additional additional VARCHAR(255) DEFAULT NULL, CHANGE source_video source_video VARCHAR(255) DEFAULT NULL');
    }
}
