<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ProductController extends AbstractController
{

  #[Route(
    path: '/products',
    name: 'products_list',
    methods: "GET"
  )]
  public function getProducts(
    Request $request,
    ProductRepository $productRepository
  ): Response {
    $pageNumber = intval($request->query->get(('pageNumber'))) ?? 0;
    $pageSize = intval($request->query->get('pageSize')) ?? 10;
    $filter = $request->query->get('filter') ?? '';

    $products = $productRepository->getProductsPaginatedWithFilter(
      $filter,
      $pageNumber + 1,
      $pageSize
    );
    return new Response(json_encode($products), 200);
  }
}
