<?

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{


  public function load(ObjectManager $objectManager)
  {

    $productsJson = file_get_contents(__DIR__ . '/../../assets/table_data.json');
    $productsData = json_decode($productsJson, true);

    foreach ($productsData as $productData) {
      $product = new Product();
      $product->setGtin($productData['gtin']);
      $product->setProductName($productData['product_name']);
      $product->setProductDesc($productData['product_desc']);
      $product->setPrice(floatval($productData['price']));
      $product->setCurrency($productData['currency']);
      $product->setCategory($productData['category']);
      $product->setGender($productData['gender']);
      $product->setQuantity(intval($productData['quantity']));
      $product->setSize($productData['size']);
      $product->setStyle($productData['style']);
      $product->setColor($productData['color']);
      $product->setUrl($productData['url']);
      $product->setImage($productData['image']);
      $product->setImageAdditional($productData['image_additional']);
      $product->setAdditional($productData['additional']);
      $product->setSourceVideo($productData['source_video']);

      $objectManager->persist($product);
    }

    $objectManager->flush();
  }
}
