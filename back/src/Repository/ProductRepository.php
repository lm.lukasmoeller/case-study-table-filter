<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function add(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getProductsPaginatedWithFilter(string $filter, int $page, int $pageSize)
    {

        $query = $this->createQueryBuilder('p');

        if ($filter !== '') {
            $query->andWhere('p.id LIKE :filter')
                ->orWhere('p.gtin LIKE :filter')
                ->orWhere('p.product_name LIKE :filter')
                ->orWhere('p.product_desc LIKE :filter')
                ->orWhere('p.price LIKE :filter')
                ->orWhere('p.currency LIKE :filter')
                ->orWhere('p.category LIKE :filter')
                ->orWhere('p.gender LIKE :filter')
                ->orWhere('p.quantity LIKE :filter')
                ->orWhere('p.size LIKE :filter')
                ->orWhere('p.style LIKE :filter')
                ->orWhere('p.color LIKE :filter')
                ->orWhere('p.url LIKE :filter')
                ->orWhere('p.image LIKE :filter')
                ->orWhere('p.image_additional LIKE :filter')
                ->orWhere('p.additional LIKE :filter')
                ->orWhere('p.source_video LIKE :filter')
                ->setParameters(['filter' => '%' . $filter . '%']);
        }

        $query->orderBy('p.id', 'ASC')->getQuery();

        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);

        $totalItems = count($paginator);

        $products = $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1))
            ->setMaxResults($pageSize)
            ->getArrayResult();

        return ['products' => $products, 'totalItems' => $totalItems];
    }
}
